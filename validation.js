//VALIDATION
const Joi = require('@hapi/joi');
//Register Validation
const registerValidation = (data) => {
    const validationSchema = Joi.object({
        firstName: Joi.string().min(2).required(),
        lastName:  Joi.string().min(2).required(),
        username:  Joi.string().min(1).required(),
        password:  Joi.string().min(6).required(),
        email:     Joi.string().min(2).required().email(),
    })
    return validationSchema.validate(data)
}

//Login Validation
const loginValidation = (data) => {
    const validationSchema = Joi.object({

        email:     Joi.string().min(2).required().email(),
        password:  Joi.string().min(6).required()
    })
    return  validationSchema.validate(data)
}

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;