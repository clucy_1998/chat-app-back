const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

    firstName: {
        type: String,
        required: true,
        min: 2
    },
    lastName: {
        type: String,
        required: true,
        min: 2
    },
    email: {
        type: String,
        required: true,
        max: 255,
        min: 6
    },
    username: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true,
        max: 1024,
        min: 6
    },
    friends: [
        {
            type: mongoose.Schema.Types.ObjectId,
        }
    ],
    conversations: [
        {
            type: mongoose.Schema.Types.ObjectId,
        }
    ],
    posts: [
        {
            content: {
                type: String
            },
            img: {
                data: Buffer,
                type: String
            },
            likes: [
                {
                    type: mongoose.Schema.Types.ObjectId,
                }
            ],
            postedAt: { type: String }
        }
    ],
    about: {
        type: String
    },
    img: {
        data: Buffer,
        type: String
    }

})

module.exports = mongoose.model('UserModel', userSchema)