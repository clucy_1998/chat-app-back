const mongoose = require('mongoose');

const messageSchema = new mongoose.Schema({

    sender: {
        type: mongoose.Schema.Types.ObjectId,
    },

    receiver: {
        type: mongoose.Schema.Types.ObjectId,
    },
    content: {
        type: String,
    },

    date: Date

})

module.exports = mongoose.model('MessageModel', messageSchema)