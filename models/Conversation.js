const mongoose = require('mongoose');
const conversationSchema = new mongoose.Schema({

    messages: [
        {
            sender: {
                type: mongoose.Schema.Types.ObjectId,
            },

            receiver: {
                type: mongoose.Schema.Types.ObjectId,
            },
            content: {
                type: String,
            },

            sentAt: { type: Date, default: Date.now }
        }
    ],
    users: {
        "user1": {
            type: mongoose.Schema.Types.ObjectId,
        },

        "user2": {
            type: mongoose.Schema.Types.ObjectId,
        }
    }
}

)

module.exports = mongoose.model('ConversationModels', conversationSchema)