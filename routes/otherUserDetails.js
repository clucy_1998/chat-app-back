const router = require('express').Router();
const UserModel = require('../models/User');
const verifyMiddleware = require('./verifyToken');
const fs = require("fs")


router.get('/photo', verifyMiddleware, async (req, res) => {
    console.log(req.query.id)
    const file = `${__dirname}/../public/img/users/${req.query.id}.jpg`;
    try {
        await fs.promises.access(file);
        res.download(file);
        // The check succeeded
    } catch (error) {
        // The check failed
        console.log(error)
        const defaultavatar = `${__dirname}/../public/img/users/default.jpg`;
        res.download(defaultavatar);
    }



})
module.exports = router;