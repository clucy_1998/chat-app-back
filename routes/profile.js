const router = require('express').Router();
const verifyMiddleware = require('./verifyToken');
const UserModel = require('../models/User');
const multer = require('multer')
const io = require('../index').io
const jwt = require('jsonwebtoken')
const fs = require("fs")


const storage = multer.diskStorage({

    destination: (req, file, cb) => {
        cb(null, 'public/img/posts')
    },
    filename: (req, file, cb) => {
        cb(null, req.user._id + `${Date.now()}` + '.jpg')
    }
})

var moment = require('moment');
var upload = multer({ storage: storage }).single('file');

router.post('/posts', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.user._id);
    upload(req, res, function (err) {
        if (req.file !== void (0)) {
            let imgName = `${req.file.filename}`;
            let postText = req.body.content;
            user.posts.push({
                content: postText,
                img: imgName,
                postedAt: moment().format('MMMM Do YYYY, h:mm a')
            });
            user.save();
        } else if (req.file === void (0)) {
            let postText = req.body.content;

            user.posts.push({
                content: postText,
                img: "",
                postedAt: moment().format('MMMM Do YYYY, h:mm a')
            });
            user.save();
        }
        if (err instanceof multer.MulterError) {
            return res.status(500).json(err)
            console.log(err)
        } else if (err) {
            return res.status(500).json(err)
        }
    })
    res.send("POSTED")
});

//Delete posts
router.delete('/posts', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.user._id);
    const postIndex = user.posts.indexOf(user.posts.find(p => p._id == req.query.post));
    user.posts.splice(postIndex, 1);
    user.save();
    res.send("DELETED")
});
//Get posts

router.get('/posts', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.user._id);
    res.send(user.posts);

});

//Get Img posts

router.get('/imgposts', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.user._id);
    userPosts = user.posts;
    const post = userPosts.find(p => p._id == req.query.id);
    const file = `${__dirname}\\..\\public\\img\\posts\\${post.img}`;
    // res.download(file);

    try {
        await fs.promises.access(file);
        res.download(file);
        // The check succeeded
    } catch (error) {
        // The check failed
        console.log(error)
        res.send("no image for this post")
    }
});

//Get Likes

router.get('/likes', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.user._id);
    const postId = req.query.postId;
    //Find the post
    const post = user.posts.find(p => p._id == postId);
    //Find num of likes
    const numOfLikes = post.likes.length;

    //Find persons who liked the post
    let userList = [];
    for (let id of post.likes) {
        let person = await UserModel.findById(id);
        userList.push(person.username)
    };

    res.send({ numOfLikes: numOfLikes, userList: userList })





});

module.exports = router;