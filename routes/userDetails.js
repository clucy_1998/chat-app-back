const router = require('express').Router();
const UserModel = require('../models/User');
const verifyMiddleware = require('./verifyToken');


router.get('/', verifyMiddleware, async (req, res) => {

    const user = await UserModel.findOne({ _id: req.user._id });
    res.json({
        username: user.username,
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email
    });

})

router.get('/photo', verifyMiddleware, async (req, res) => {

    const file = `${__dirname}/../public/img/users/${req.user._id}.jpg`;
    res.download(file);

})
module.exports = router;