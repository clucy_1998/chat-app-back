const router = require('express').Router();
const UserModel = require('../models/User');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const { registerValidation, loginValidation } = require('../validation')



router.post('/register', async (req, res) => {

    //VALIDATE THE DATA BEFORE INSERTING A NEW USER
    const { error } = registerValidation(req.body);
    if (error) return res.status(400).send(error.details[0].message);


    //Hash password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);
    //Check if the User already exists
    const emailExist = await UserModel.findOne({ email: req.body.email });
    if (emailExist) return res.status(400).send('A user with this email already exists')

    const user = new UserModel({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        username: req.body.username,
        password: hashedPassword,
    });

    try {
        const savedUser = await user.save();
        res.send({
            user: savedUser.username
        });
    } catch (err) {
        res.status(400).send(err)
    }

})

//LOGIN ROUTE

router.post('/login', async (req, res) => {

    const { error } = loginValidation(req.body);

    if (error) return res.status(400).send(error.details[0].message);
    //Checking if the email exists
    const user = await UserModel.findOne({ email: req.body.email });
    if (!user) return res.status(400).send('Invalid credentials');
    //Check if password is correct
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass) return res.status(400).send('Invalid credentials');

    //Create and assign a token
    const token = jwt.sign({ _id: user._id }, process.env.JWT_KEY);
    //res.header('auth-token', token).send(token);
    res.cookie('token', token, { expires: false, httpOnly: true, secure: false });
    res.status(200).send("OK")

});

module.exports = router;