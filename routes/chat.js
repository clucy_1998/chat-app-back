const router = require('express').Router();
const verifyMiddleware = require('./verifyToken');
const UserModel = require('../models/User');
const ConversationModel = require('../models/Conversation');
const io = require('../index').io
const jwt = require('jsonwebtoken')

//SOCKET MESSAGES PART


io.on('connection', client => {
    client.on("chat message", async (message) => {
        const cookie = client.handshake.headers.cookie;
        const token = cookie.split("=")[1];
        console.log(client.handshake.headers.cookie)

        const decodedToken = jwt.verify(token, process.env.JWT_KEY);//id + expire
        const user = decodedToken._id;
        const receiver = message.receiver;
        const content = message.content;

        console.log(message);

        let newMessageMap = [];
        let newMessageObject = {};
        newMessageObject["sender"] = user;
        newMessageObject["receiver"] = receiver;
        newMessageObject["content"] = content;
        newMessageMap.push(newMessageObject);
        //Emit the message back 
        io.sockets.emit('new message', [newMessageMap, { user: user }]);
        //Save the message in Database
        let docs = await ConversationModel.find({
            $or: [
                {
                    'users.user1': user,
                    'users.user2': receiver

                },
                {
                    'users.user1': receiver,
                    'users.user2': user

                }

            ]
        });
        docs[0].messages.push({
            sender: user,
            receiver: receiver,
            content: content
        });
        docs[0].save();


        //Later when database, if there's a conversation containing

    });



    //Disconnet
    client.on('disconnect', () => {

        console.log('User has left')
    });
});


router.get('/users', verifyMiddleware, async (req, res) => {
    var userMap = [];
    let filteredUsers = [];
    await UserModel.find({}, (err, users) => {

        users.forEach((user) => {
            userMap.push(user);
        });

    });

    for (const user of userMap) {
        let file = `${__dirname}/../public/img/users/${user._id}.jpg`;
        let filteredUser = {};
        filteredUser["_id"] = user._id
        filteredUser["username"] = user.username;
        filteredUser["firstName"] = user.firstName;
        filteredUser["lastName"] = user.lastName;
        //  filteredUser["img"] = file
        filteredUsers.push(filteredUser);
        res.download(file);
    }
    //console.log(files)
    res.send(filteredUsers);

});

router.get('/conversations', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.user._id);

    let conversationsInUser = [];
    conversationsInUser = user.conversations;

    // console.log(conversationsInUser);
    let conversationsToSend = [];
    let conversations = await ConversationModel.find({
        $or: [
            {
                'users.user1': user._id
            },
            {
                'users.user2': user._id
            }

        ]
    });

    conversations.forEach((conversation) => {
        if (conversationsInUser.includes(conversation._id)) {
            conversationsToSend.push(conversation);
        }
    })
    // console.log("AICII")
    // console.log(conversationsToSend)


    let persons = [];
    conversationsToSend.forEach((conversation) => {
        for (var key of Object.keys(conversation.users)) {
            if ((conversation.users[key]).toString() != (user._id.toString())) {
                persons.push(conversation.users[key]);
            }
        }
    })
    //console.log(persons)
    //Now I have the persons, it's time to look them up in the database and retrieve the info to be diplayed in the Contact List
    let personList = [];
    for (let person of persons) {
        if (person != true) {
            let contact = await UserModel.findById(person);
            personList.push(contact)
        }

    };
    let filteredUsers = [];
    for (const person of personList) {
        let file = `${__dirname}/../public/img/users/${person._id}.jpg`;
        let filteredUser = {};
        filteredUser["_id"] = person._id
        filteredUser["username"] = person.username;
        filteredUser["firstName"] = person.firstName;
        filteredUser["lastName"] = person.lastName;
        //  filteredUser["img"] = file
        filteredUsers.push(filteredUser);
        // res.download(file);
    }
    //  console.log(filteredUsers);
    res.send(filteredUsers)
});

router.post('/conversations', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.user._id);
    let userId = user._id;
    let contactId = req.body._id;
    const contact = await UserModel.findById(contactId);

    const docs = await ConversationModel.find({
        $or: [
            {
                'users.user1': userId,
                'users.user2': contactId

            },
            {
                'users.user1': contactId,
                'users.user2': userId

            }

        ]
    });

    //Id the conversation doesn't exist, creat a new one
    if (docs === undefined || docs.length == 0) {
        //Create a new Conversation
        let conversation = new ConversationModel({
            users: {
                user1: userId,
                user2: contactId
            }
        });
        const savedConversation = await conversation.save();
        if (!user.conversations.includes(savedConversation._id)) {
            //Push into the user
            user.conversations.push(savedConversation._id);
            user.save();
        }
        if (!contact.conversations.includes(savedConversation._id)) {
            //Push into the contact
            contact.conversations.push(savedConversation._id);
            contact.save();
        }

    } else {
        const convId = docs[0]._id;
        if (!user.conversations.includes(convId)) {
            //Push into the user
            user.conversations.push(convId);
            user.save();
        }
        if (!contact.conversations.includes(convId)) {
            //Push into the contact
            contact.conversations.push(convId);
            contact.save();
        }

    }




    res.status(201).send();
});

//Get the messages
router.get('/conversations/messages', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.user._id);
    const contact = req.query.contactId;
    if (contact != "") {
        const conversation = await ConversationModel.find({
            $or: [
                {
                    'users.user1': user._id,
                    'users.user2': contact
                },
                {
                    'users.user1': contact,
                    'users.user2': user._id
                }

            ]
        });
        try {
            const messages = conversation[0].messages;
            res.send([messages, { user: user._id }])
        }
        catch{
            //no messages
            res.send([{}, { user: user._id }])
        }



    }


});

//Delete a conversation
router.post('/conversations/delete', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.user._id);
    let userId = user._id;
    let contactId = req.body._id;

    const docs = await ConversationModel.find({
        $or: [
            {
                'users.user1': userId,
                'users.user2': contactId

            },
            {
                'users.user1': contactId,
                'users.user2': userId

            }

        ]
    });
    // for (var i = 0; i < user.conversations.length; i++) {

    //     if (user.conversations[i] === docs[0]._id) {
    //         user.conversations.splice(i, 1);
    //         user.save()
    //     }

    // }
    user.conversations = user.conversations.filter(c => {
        return c._id === docs[0].id;
    });
    user.save();

    // console.log("ACII PT DELETE");
    // ConversationModel.findById(docs[0]._id, function (err, doc) {
    //     if (err) {
    //         console.log(err);
    //     }

    //     doc.remove(() => {
    //         console.log("removed succefully")
    //     }); //Removes the document
    // })

    res.status(201).send();
});

module.exports = router;