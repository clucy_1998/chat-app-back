const router = require('express').Router();
const verifyMiddleware = require('./verifyToken');
const UserModel = require('../models/User');
const multer = require('multer')
const bcrypt = require('bcryptjs');


const storage = multer.diskStorage({

  destination: (req, file, cb) => {
    console.log("Hello people in storage");
    cb(null, 'public/img/users')
  },
  filename: (req, file, cb) => {
    cb(null, req.user._id + '.jpg')
  }
})



var upload = multer({ storage: storage }).single('file');

router.post('/photo', verifyMiddleware, (req, res) => {
  upload(req, res, function (err) {

    if (err instanceof multer.MulterError) {
      return res.status(500).json(err)
    } else if (err) {
      return res.status(500).json(err)
    }
  })
  var token = req.cookies["token"]

  var path = "public/img/users/" + `${req.user._id}` + ".jpg";

  UserModel.findByIdAndUpdate(req.user._id, { img: path }, (err, result) => {
    if (err) {
      // console.log(err)
    } else {
      //  console.log(result)
    }
  })

  return res.status(200).send("IMAGE UPLOADED")

})

router.post('/data', verifyMiddleware, async (req, res) => {
  console.log(req.body);
  const username = req.body.usernameForm;
  const firstName = req.body.firstNameForm;
  const lastName = req.body.lastNameForm;
  const email = req.body.emailForm;
  const password = req.body.passwordForm;


  if (username) {
    UserModel.findByIdAndUpdate(req.user._id,
      {
        username: username,

      }, (err, result) => {
        if (err) {
          res.send(err);
        } else {
          res.status(200);
        }
      })
  }

  if (firstName) {
    UserModel.findByIdAndUpdate(req.user._id,
      {
        firstName: firstName,

      }, (err, result) => {
        if (err) {
          res.send(err);
        } else {
          res.status(200);
        }
      })
  }

  if (lastName) {
    UserModel.findByIdAndUpdate(req.user._id,
      {
        lastName: lastName,

      }, (err, result) => {
        if (err) {
          res.send(err);
        } else {
          res.status(200);
        }
      })
  }

  if (email) {
    UserModel.findByIdAndUpdate(req.user._id,
      {
        email: email,

      }, (err, result) => {
        if (err) {
          res.send(err);
        } else {
          res.status(200);
        }
      })
  }

  if (password) {
    //Hash password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    UserModel.findByIdAndUpdate(req.user._id,
      {
        password: hashedPassword,

      }, (err, result) => {
        if (err) {
          res.send(err);
        } else {
          res.status(200);
        }
      })
  }
});


module.exports = router;