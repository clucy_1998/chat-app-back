const router = require('express').Router();
const verifyMiddleware = require('./verifyToken');
const UserModel = require('../models/User');
const multer = require('multer')
const io = require('../index').io
const jwt = require('jsonwebtoken');
const User = require('../models/User');
//Get posts

router.get('/posts', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.query.id);
    // if (user.posts === undefined || user.posts.length == 0) {
    //     res.send()
    // }
    res.send(user.posts);

});

//Get Img posts

router.get('/imgposts', verifyMiddleware, async (req, res) => {
    const user = await UserModel.findById(req.query.userId);
    userPosts = user.posts;
    const post = userPosts.find(p => p._id == req.query.postId);
    const file = `${__dirname}\\..\\public\\img\\posts\\${post.img}`;
    res.download(file);


});

router.post('/likes', verifyMiddleware, async (req, res) => {
    //gotta receive the otheruser and the id of the post 
    const userOfPost = await UserModel.findById(req.body.userId);
    const postId = req.body.postId;
    const userId = req.user._id;

    //Find the post
    const post = userOfPost.posts.find(p => p._id == postId);
    const postIndex = userOfPost.posts.indexOf(post);

    //Check if user already liked

    if (post.likes.includes(userId)) {
        //DELETE THE LIKE
        post.likes.remove(userId);
        console.log(post)
        //Splice the posts and inser the new post
        userOfPost.posts.splice(postIndex, 1, post);
        userOfPost.save();

        res.send("POST DISLIKED")

    } else {
        //Modify the likes
        post.likes.push(userId);
        console.log(post)

        //Splice the posts and inser the new post
        userOfPost.posts.splice(postIndex, 1, post);
        userOfPost.save();

        res.send("POST LIKED")
    }


})

// router.post('/dislikes', verifyMiddleware, async (req, res) => {
//     //gotta receive the otheruser and the id of the post 
//     const userOfPost = await UserModel.findById(req.body.userId);
//     const postId = req.body.postId;
//     const userId = req.user._id;

//     //Find the post
//     const post = userOfPost.posts.find(p => p._id == postId);
//     const postIndex = userOfPost.posts.indexOf(post);

//     //Check if user already liked it

//     if (post.likes.includes(userId)) {
//         //Dislike it 

//         post.likes.remove(userId);
//         console.log(post)
//         //Splice the posts and inser the new post
//         userOfPost.posts.splice(postIndex, 1, post);
//         userOfPost.save();

//         res.send("POST DISLIKED")
//     } else {
//         res.send("NOT LIKED")
//     }


// })
module.exports = router;