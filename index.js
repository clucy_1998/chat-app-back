const express = require('express');
var cors = require('cors')
const app = express();
const dotenv = require('dotenv');
const path = require('path');
const mongoose = require('mongoose');
var cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const http = require('http');
var session = require('express-session');
dotenv.config();
// const formData = require("express-form-data");


//app.use(formData.parse());
app.use(cookieParser());
app.use(bodyParser.json());
// app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: true }));


//express.static(__dirname + '/public')
app.use(express.static(path.join(__dirname, 'public')))

app.use(cors({
  exposedHeaders: ['Content-Length', 'Set-Cookie'],
  credentials: true,
  origin: 'http://localhost:3000'
}));

//Socket
let server = http.createServer(app);
let io = exports.io = require('socket.io')(server, {
  transports: ["websocket", "polling"]
});
//App listen
const port = process.env.PORT || 5000;
server.listen(port, () => console.log('Server running'));

//Import Routes
const authRoute = require('./routes/auth');
const chatRoute = require('./routes/chat');
const settingsRoute = require('./routes/settings');
const userDetails = require('./routes/userDetails');
const otherUserDetails = require('./routes/otherUserDetails');
const userProfile = require('./routes/profile');
const otherProfile = require('./routes/otherProfile');


//Connect to DB
var mongoDB = 'mongodb://127.0.0.1/chatApp';
mongoose.connect(mongoDB, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

//MiddleWare
app.use(express.json());



//Route MiddleWares
app.use('/api/user', authRoute);
app.use('/api/chat', chatRoute);
app.use('/api/user/settings', settingsRoute);
app.use('/api/user/userDetails', userDetails);
app.use('/api/user/otherUserDetails', otherUserDetails);
app.use('/api/user/profile', userProfile);
app.use('/api/user/otherProfile', otherProfile);

